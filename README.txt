/**
 *  Cross Chinese search
 *  Update: 2012/02/10
 */


Summary
-------
This project is used for Drupal core search module and provide 
"Traditional Chinese" and "Simplified Chinese" cross search. 
Example, if you have some of the content contains the word  
"資料庫", and you're enter like "资料库", "资料庫" or "数据库"... 
all can be finded.

This module is base on Mediawiki (http://www.mediawiki.org/wiki/MediaWiki)
and mediawiki-zhconverter which is from google project (http://code.google.com/p/mediawiki-zhconverter/)
Demo: http://labs.xddnet.com/mediawiki-zhconverter/example/example.html



Requirements and Dependencies
-----------------------------
 * Chinese Word Splitter(中文分词)    --- http://drupal.org/project/csplitter
 * Mediawiki ZH Converter             --- http://drupal.org/project/mediawiki_zhconverter


Installation and Settings
-------------------------
To successful completion of this installation, according to the following steps:

1) Download and enable above dependencies module and follow those steps to success install.

2) Enable this module.
   

Author
------
Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)
If you use this module, find it useful, and want to send the author a thank 
or you note, feel free to contact me.

The author can also be contacted for paid customizations of this and other modules.